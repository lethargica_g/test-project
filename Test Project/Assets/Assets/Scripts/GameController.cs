﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

[Serializable]
public class Count
{
    public float minimum;
    public float maximum;

    public Count (float min, float max) {
        minimum = min; 
        maximum = max;
    }
}

public class GameController : MonoBehaviour
{


    private Count posX = new Count(-1, 1);
    private Count posY = new Count(-1, 1);

    [SerializeField] private PlayerController player;
    [SerializeField] private Count circleStartSize = new Count(0.5f, 1);
    [SerializeField] private float circleMoveForce;
    [SerializeField] private float playerStartSize;

    private Color minColor;
    private Color maxColor;
    private float radiusDelta = 0.1f;
    private float currentPlayerSqr;
    private float enemySqr;

    private bool spawned = false;
    private bool findFreePosition;

    void Start()
    {
        string configText = Resources.Load<TextAsset>("GameConfig").text;
        GameConfig gameConfig = JsonUtility.FromJson<GameConfig>(configText);
        minColor = Color.white;
        maxColor = Color.white;
        Color playerColor = Color.white;
        for (int i = 0; i < 3; i++)
        {
            minColor[i] = gameConfig.enemy.color1[i] / 255f;
            maxColor[i] = gameConfig.enemy.color2[i] / 255f;
            playerColor[i] = gameConfig.user.color[i] / 255f;
        }
        player.gameObject.SetActive(false);
        player.Radius = playerStartSize;
        player.CurrentColor = playerColor;
        player.onRadiusChanged += OnPlayerRadiusChanged;
        currentPlayerSqr = player.PlayerSqr;
        GetScreenPositions();
    }

    public void StartGame()
    {
        player.gameObject.SetActive(true);
        StartCoroutine(InIt());
    }

    public void Restart()
    {
        ResetScene();
        player.Reset(playerStartSize);
        currentPlayerSqr = player.PlayerSqr;
        GameManager.instance.StartGame();
    }

    IEnumerator InIt()
    {
        spawned = false;
        while (Pool.poolItems.Count > 0)
        {
            PasteFromPool();
            yield return new WaitUntil((() => findFreePosition));
        }
        spawned = true;
    }

    void GetScreenPositions()
    {
        float height = 2f * Camera.main.orthographicSize;
        float width = height * Camera.main.aspect;
        posX.minimum = -width / 2 + 0.5f;
        posX.maximum = width / 2 - 0.5f;
        posY.minimum = -height / 2 + 0.5f;
        posY.maximum = height / 2 - 0.5f;
    }

    void PasteFromPool()
    {
        GameObject obj = Pool.poolItems.Dequeue();
        Pool.inUse.Add(obj.GetComponent<CircleController>());
        float radius = Random.Range(circleStartSize.minimum, circleStartSize.maximum);
        Vector2 randomPosition = GetRandomPosition(radius);
        float t = (radius - player.Radius / 2f) / (1.5f * player.Radius);
        Color circleColor = Color.Lerp(minColor, maxColor, t);
        CircleController circle = obj.GetComponent<CircleController>();
        circle.minSizeColor = minColor;
        circle.maxSizeColor = maxColor;
        enemySqr += Mathf.PI * Mathf.Pow(radius, 2);
        circle.SetUp(radius, circleMoveForce, randomPosition, circleColor);

        obj.SetActive(true);
    }

    Vector2 GetRandomPosition(float radius)
    {
        findFreePosition = false;
        Vector2 position = new Vector2();
        while (!findFreePosition)
        {
            position = new Vector2(Random.Range(posX.minimum, posX.maximum), Random.Range(posY.minimum, posY.maximum));
            if (!Physics2D.OverlapCircle(position, (radius + radiusDelta)))
            {
                findFreePosition = true;
                break;
            }
        }
        return position;
    }

    void OnPlayerRadiusChanged(float playerRadius)
    {
        for (int i = 0; i < Pool.inUse.Count; i++)
        {
            Pool.inUse[i].OnPlayerRadiusChanged(playerRadius);
        }
        float sqr = Mathf.PI * Mathf.Pow(playerRadius, 2);
        enemySqr -= (sqr - currentPlayerSqr);
        currentPlayerSqr = sqr;
        if (spawned)
        {
            if (currentPlayerSqr > enemySqr)
            {
                GameManager.instance.win = true;
                GameManager.instance.EndGame();
            } 
        }
    }

    public void EndGame()
    {
        List<CircleController> inUse = Pool.inUse;
        foreach (CircleController circle in inUse)
        {
            circle.Stop();
        }
    }

    void ResetScene()
    {
        enemySqr = 0;
        while (Pool.inUse.Count > 0)
        {
            Pool.ReturnToPool(Pool.inUse[0].gameObject);
        }
    }
}