﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using UnityEngine;
using Random = UnityEngine.Random;

public class CircleController : Circle
{
	
	private Vector2 movement;
	private float moveForce;

	public Color minSizeColor;
	public Color maxSizeColor;
	
	void OnEnable()
	{
		Move(movement, moveForce);
	}

	public void SetUp(float radius, float force, Vector2 startPosition, Color color)
	{
		gameObject.transform.position = startPosition;
		Radius = radius;
		CurrentColor = color;
		moveForce = force;
		movement = Random.insideUnitCircle.normalized;
	}
	
	protected override void Move(Vector2 direction, float force)
	{
//		Rb2D.AddForce(direction * force, ForceMode2D.Impulse);
		Rb2D.velocity = direction * force;
	}

	public void OnPlayerRadiusChanged(float playerRadius)
	{
		float t = (Radius - playerRadius / 2f) / (1.5f * playerRadius);
		Color circleColor = Color.Lerp(minSizeColor, maxSizeColor, t);
	}

	public void Stop()
	{
		Rb2D.velocity = Vector2.zero;
	}
}
