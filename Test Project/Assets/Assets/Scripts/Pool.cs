﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Pool : MonoBehaviour
{
	public static Queue<GameObject> poolItems = new Queue<GameObject> ();
	public static List<CircleController> inUse = new List<CircleController>();

	public GameObject circleItem;
	public int count;

	private Transform holder;

	private void Start()
	{
		poolItems = CreatePool(circleItem, count);
	}

	Queue<GameObject> CreatePool (GameObject obj, int itemsCount)
	{
		holder = new GameObject ("Holder").transform;
		Queue<GameObject> queue = new Queue<GameObject> ();

		for (int i = 0; i < itemsCount; i++)
		{
			GameObject objectToSpawn = Instantiate (circleItem, holder.position, Quaternion.identity) as GameObject;
			objectToSpawn.transform.SetParent (holder, false);
			objectToSpawn.SetActive (false);
			queue.Enqueue (objectToSpawn);
		}
		return queue;
	}

	public static void ReturnToPool(GameObject obj)
	{
		obj.SetActive(false);
		poolItems.Enqueue(obj);
		inUse.Remove(obj.GetComponent<CircleController>());
	}
}