﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.WebCam;

public abstract class Circle : MonoBehaviour
{
	
	public Action<float> onRadiusChanged;

	
	[SerializeField] private Transform sprite;
	[SerializeField] protected SpriteRenderer sr;

	private Rigidbody2D rb2D;
	private CircleCollider2D collider;
	
	public Color CurrentColor
	{
		set { sr.color = value; }
	}

	protected Rigidbody2D Rb2D
	{
		get { return rb2D ?? (rb2D = GetComponent<Rigidbody2D>()); }
	}

	protected CircleCollider2D Collider
	{
		get { return collider ?? (collider = GetComponent<CircleCollider2D>()); }
	}

	public float Radius
	{
		get { return Collider.radius; }
		set
		{
			Collider.radius = value;
			sprite.localScale = new Vector2(value * 2, value * 2);
			if (onRadiusChanged != null)
				onRadiusChanged.Invoke(value);
		}
	}

	protected abstract void Move(Vector2 direction, float moveForce);
	
	protected void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Circle"))
		{
			CheckSqr(other.gameObject);
		} 
	}

	void CheckSqr(GameObject enemy)
	{
		float enemyRadius = enemy.GetComponent<CircleCollider2D>().radius;
		if (Radius > enemyRadius)
		{
			float currentSquare = Mathf.PI * Mathf.Pow(Radius, 2);
			float otherSquare = Mathf.PI * Mathf.Pow(enemyRadius, 2);
			currentSquare += otherSquare;
			Radius = Mathf.Sqrt(currentSquare / Mathf.PI);

			enemy.GetComponent<Circle>().OnDeath();
		}
		else
		{
			if (gameObject.CompareTag("Player"))
			{
				GameManager.instance.win = false;
				GameManager.instance.EndGame();
				gameObject.SetActive(false);
			}
		}
	}

	public void OnDeath()
	{
		Pool.ReturnToPool(gameObject);
	}
}
