﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{
    public const string loseText = "you lose!";
    public const string winText = "you win!";
}
