﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public Text finishGameText;
    public GameObject playButton;
    public GameObject restartButton;
    public GameObject blockedImage;

    void Start()
    {
        playButton.SetActive(true);
        restartButton.SetActive(false);
        finishGameText.gameObject.SetActive(false);
        blockedImage.SetActive(true);
    }

    public void InGame()
    {
        playButton.SetActive(false);
        restartButton.SetActive(true);
        finishGameText.gameObject.SetActive(false);
        blockedImage.SetActive(false);
    }
    
    public void FinishGame()
    {
        finishGameText.text = GameManager.instance.win ? GameData.winText.ToUpper(): GameData.loseText.ToUpper();
        finishGameText.gameObject.SetActive(true);
        blockedImage.SetActive(true);
    }
}
