﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Circle
{
	
	public float moveForce = 4f;
	private Vector2 clickPosition;

	public float PlayerSqr
	{
		get { return Mathf.PI * Mathf.Pow(Radius, 2); }
	}
	
	private void FixedUpdate()
	{
		if (Input.GetMouseButton(0))
		{
			Vector3 mousPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 movementDirection = transform.position - mousPosition;
			Move(movementDirection, moveForce);
		}
	}
	
	protected override void Move(Vector2 direction, float moveForce)
	{
		Rb2D.AddForce(direction * moveForce * Time.deltaTime);
	}

	public void Reset(float size)
	{
		Radius = size;
		transform.position = Vector3.zero;
	}
}
