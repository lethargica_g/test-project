﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	public UnityEvent onStartGame;
	public UnityEvent onEndGame;
	public UnityEvent onRestartGame;

	bool isGameOver;

	public bool win;

	void Awake () {
		instance = this;
	}
	
	public void StartGame () {
		onStartGame.Invoke ();
	}
	
	public void EndGame () {
		isGameOver = true;
		onEndGame.Invoke ();
	}

	public void RestartGame()
	{
		isGameOver = false;
		onRestartGame.Invoke();
	}
}
