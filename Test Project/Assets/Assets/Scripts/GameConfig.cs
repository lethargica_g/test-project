﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerConfig
{
    public int[] color;
}

[System.Serializable]
public class EnemyConfig
{
    public int[] color1;
    public int[] color2;
}

[System.Serializable]
public class GameConfig
{
    public PlayerConfig user;
    public EnemyConfig enemy;
}

